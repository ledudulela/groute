#!/usr/bin/python3
# -*- coding: UTF-8 -*-
SCRIPT_VERSION=20210526.1934
SCRIPT_AUTHOR="ledudulela"
# 
L_EN="EN"
L_FR="FR"
VALID_L=(L_FR,L_EN)
gLOCALE=L_EN # default language
# LOCALE is redefined according to the language of the help file located in the directory of the application. 
# Remove (or rename) unnecessary help files.
# LOCALE est redéfinie en fonction de la langue du fichier d'aide se trouvant dans le répertoire de l'application. 
# Supprimer (ou renommer) les fichiers d'aide inutiles.
#
# --- ENGLISH ---
# GUI of the command line script (same file name without _ui).
# Requires package python3-tk

# --- FRANCAIS ---
# Interface graphique du script en ligne de commandes (même nom de fichier sans _ui ). 
# Nécessite le package python3-tk si exécution avec python3 sinon python-tk 

# What's new in this version:
# move row with alt + 8 or alt + 2 (numpad) or Double-Left or Double-Rigth Mouse Button on L button

from sys import argv as argv
import math
import re
import json
import time
#import urllib
#from functools import partial

# importe le script de commandes
moduleCmd=argv[0].replace("_ui","")
moduleCmd=moduleCmd.replace(".py","")
moduleCmd=moduleCmd.replace("./","")
cmd=__import__(moduleCmd) # import moduleCmd as cmd

if cmd.sys.version_info.major<3:
   import Tkinter as tk
   import tkMessageBox as msgbox
   import tkFileDialog
   import tkSimpleDialog as inputbox
   import ttk
   import urllib2
else:
   import tkinter as tk
   from tkinter import messagebox as msgbox
   import tkinter.filedialog as tkFileDialog
   from tkinter import simpledialog as inputbox
   from tkinter import ttk
   import urllib.request as urllib2
   
REGEXCSV='(?!\B"[^"]*),(?![^"]*"\B)'

REPOSITORY_LOCAL="http://localhost/groute"
DOWNLOAD_LOCAL=REPOSITORY_LOCAL+"/%s"

REPOSITORY_GITLAB="http://gitlab.com/ledudulela/groute"
DOWNLOAD_GITLAB=REPOSITORY_GITLAB+"/-/raw/master/%s?inline=false"

REPOSITORY_URL=REPOSITORY_GITLAB
DOWNLOAD_URL=DOWNLOAD_GITLAB

DTEDURL_OPENTEST=REPOSITORY_LOCAL+"/lookup"
DTEDURL_OPENELEVATION="https://api.open-elevation.com/api/v1/lookup"
DTEDURL_OPENTOPODATA="https://api.opentopodata.org/v1/aster30m" # dem server name after .../V1/
# https://www.opentopodata.org/#public-api Max 100 locations per request, Max 1 call per second, Max 1000 calls per day.
DTEDURL_QRY=DTEDURL_OPENTOPODATA # DTEDURL_OPENELEVATION

MAGNETURL_NOAA="http://www.ngdc.noaa.gov/geomag-web/calculators/calculateDeclination"
MAGNETURL_QRY=MAGNETURL_NOAA
# ----------------------------------------------------------------------------
def i18n(strKey):
   mapping={
      "mnuApp":{L_FR:"Application",L_EN:"Application"},
      "mnuAppShowConfig":{L_FR:"Afficher la configuration", L_EN:"Display configuration"},
      "mnuAppSaveConfig":{L_FR:"Sauvegarder la configuration", L_EN:"Save configuration"},
      "mnuAppResetConfig":{L_FR:"Réinitialiser la configuration", L_EN:"Reset configuration"},
      "mnuAppOpenFile":{L_FR:"Ouvrir un fichier...", L_EN:"Open file..."},
      "mnuAppSaveContentAs":{L_FR:"Enregistrer le contenu...", L_EN:"Save content..."},
      "mnuAppHelp":{L_FR:"Aide...", L_EN:"Help..."}, 
      "mnuAppAbout":{L_FR:"À propos...", L_EN:"About..."}, 
      "mnuAppExit":{L_FR:"Quitter", L_EN:"Exit"}, 
      "mnuData":{L_FR:"B.D.D",L_EN:"D.B"},
      "mnuDataShowDBFile":{L_FR:"Ouvrir la sélection", L_EN:"Open selection"},
      "mnuDataConvertDBFileToCSV":{L_FR:"Convertir en CSV",L_EN:"Convert in CSV"},
      "mnuDataExtractDBFile":{L_FR:"Extraire une zone...",L_EN:"Extract a zone..."}, 
      "mnuDataSaveAsDBFileList":{L_FR:"Enregistrer sous...", L_EN:"Save as..."},
      "mnuDataDeleteDBFile":{L_FR:"Supprimer...", L_EN:"Delete..."},
      "mnuDataRefreshDBFileList":{L_FR:"Actualiser la liste", L_EN:"Refresh list"},
      "mnuSearch":{L_FR:"Recherche",L_EN:"Search"},
      "mnuSearchSearch":{L_FR:"Rechercher les points", L_EN:"Search points"},
      "mnuSearchShowCSV":{L_FR:"Afficher en CSV (R)", L_EN:"Show in CSV (R)"},
      "mnuSearchShowGPX":{L_FR:"Afficher en GPX", L_EN:"Show in GPX"},
      "mnuSearchShowXML":{L_FR:"Afficher en XML", L_EN:"Show in XML"},
      "mnuSearchShowLog":{L_FR:"Afficher le Log", L_EN:"Show Log"},
      "mnuEditContent":{L_FR:"Edition", L_EN:"Edit"},
      "mnuEditContentCopy":{L_FR:"Copier (alt C)", L_EN:"Copy (alt C)"},
      "mnuEditContentCut":{L_FR:"Couper (alt X)", L_EN:"Cut (alt X)"},
      "mnuEditContentPast":{L_FR:"Coller (alt V)", L_EN:"Past (alt V)"},
      "mnuEditContentCancel":{L_FR:"Annuler (alt Z)", L_EN:"Cancel (alt Z)"},
      "mnuEditContentInsertRow":{L_FR:"Insérer une ligne (alt I)", L_EN:"Insert row (alt I)"},
      "mnuEditContentCutRow":{L_FR:"Couper la ligne (alt K)", L_EN:"Cut row (alt K)"},
      "mnuEditContentSelectRow":{L_FR:"Sélectionner la ligne (alt L)", L_EN:"Select the row (alt L)"},
      "mnuEditContentSelectAll":{L_FR:"Tout sélectionner (alt A)", L_EN:"Select All (alt A)"},
      "mnuTools":{L_FR:"Outils",L_EN:"Tools"},
      "mnuToolsDist2Points":{L_FR:"Ajouter Distance & Bearing", L_EN:"Add Distance & Bearing"},
      "mnuToolsIntermediatePt":{L_FR:"> Points intermédiaires (après D. & B.)", L_EN:"> Intermediate Points (after D. & B.)"},
      "mnuToolsCoordByBearingDist":{L_FR:"Coord. à partir de: lat,lon, dist,bearing", L_EN:"Coord. from: lat,lon, dist,bearing"},
      "mnuToolsMagnetDeclin":{L_FR:"Correction du bearing géo./magnét.", L_EN:"Geo./Magnet. bearing correction"},
      "mnuToolsConvertCsvToGpx":{L_FR:"Convertir le CSV en GPX", L_EN:"Convert CSV to GPX"},
      "mnuToolsReplaceBlankRowsByHeader":{L_FR:"Remplacer les lignes vierges par l'entête", L_EN:"Replace blank rows by header"},
      "mnuToolsContentRowsCount":{L_FR:"Nombre de lignes...", L_EN:"Rows count..."},
      "mnuToolsMagneticVar":{L_FR:"Déclinaison magnétique", L_EN:"Magnetic Declination"},
      "mnuToolsMagneticVarDownload":{L_FR:"Télécharger...", L_EN:"Download..."},
      "mnuToolsMagneticVarMinMax":{L_FR:"Min & Max...", L_EN:"Min & Max..."},
      "mnuToolsMagneticVarConvertJsonToCsv":{L_FR:"Convertir le Json en CSV", L_EN:"Convert Json to CSV"},
      "mnuToolsElevation":{L_FR:"Elévation", L_EN:"Elevation"},
      "mnuToolsElevationOpenFile":{L_FR:"Ouvrir un fichier...", L_EN:"Open file..."},
      "mnuToolsElevationDownload":{L_FR:"Télécharger...", L_EN:"Download..."},
      "mnuToolsElevationMaximum":{L_FR:"Maximum...", L_EN:"Maximum..."},
      "mnuToolsElevationConvertJsonToCsv":{L_FR:"Convertir le Json en CSV", L_EN:"Convert Json to CSV"},
      "btnAppQuit":{L_FR:"Quitter",L_EN:"Quit"},
      "btnSearchSearch":{L_FR:"Rechercher",L_EN:"Search"},
      "btnToolsDist2Points":{L_FR:"+ Dist. & Bear.",L_EN:"+ Dist. & Bear."},
      "btnToolsCoordByBearingDist":{L_FR:"Coord. Pt dist.",L_EN:"Coord. dist. Pt"},
      "btnToolsMagnetDeclin":{L_FR:"Géo. / Magnét.",L_EN:"Geo. / Magnet."},
      "PopText_close":{L_FR:"[x]",L_EN:"[x]"},
      "PopText_copy":{L_FR:"Copier",L_EN:"Copy"},
      "PopText_cut":{L_FR:"Couper",L_EN:"Cut"},
      "PopText_past":{L_FR:"Coller",L_EN:"Past"},
      "PopText_cancel":{L_FR:"Annuler",L_EN:"Cancel"},
      "PopText_select_row":{L_FR:"Sélectionner la ligne",L_EN:"Select row"},
      "PopText_insert_row":{L_FR:"Insérer une ligne",L_EN:"Insert row"},
      "PopText_cut_row":{L_FR:"Couper la ligne",L_EN:"Cut row"},
      "PopText_select_all":{L_FR:"Tout sélectionner",L_EN:"Select all"},
      "PopText_rows_count":{L_FR:"Nbr de lignes...",L_EN:"Nb rows..."},
      "lblInfoLib1":{L_FR:"Base de données",L_EN:"Database"},
      "frmChoixType":{L_FR:"Points à rechercher",L_EN:"Points to search"},
      "lblInfoLib2":{L_FR:"Veuillez patentier",L_EN:"Please wait"},
      "nbkContentLib":{L_FR:"Contenu",L_EN:"Content"},
      "nbkSandboxLib":{L_FR:"Bloc-notes",L_EN:"Notepad"},
      "mnuAppHelp_msg_filenotfound":{L_FR:"Fichier introuvable.",L_EN:"File not found."},
      "mnuDataExtractDBFile_msg_info_extract":{L_FR:"Veuillez afficher, modifier et sauvegarder les paramètres de configuration avant de lancer une extraction.",L_EN:"Please open, modify and save configuration parameters before extraction."},
      "mnuToolsDist2Points_msg_info_err":{L_FR:"Format de données non valide.\nCSV sans entêtes lat,lon ou dist,bearing déjà présents.",L_EN:"Bad data format\nCSV without lat,lon header or dist,bearing already exist."},
      "mnuToolsCoordByBearingDist_msg_info_err":{L_FR:"Format de données non valide.\nCSV sans entêtes lat,lon,dist,bearing.",L_EN:"Bad data format\nCSV without lat,lon,dist,bearing header."},
      "mnuToolsMagnetDeclin_msg_info_err":{L_FR:"Format de données non valide.\nCSV sans entêtes lat,lon,dist,bearing.",L_EN:"Bad data format\nCSV without lat,lon,dist,bearing header."},
      "os_msg_open_file":{L_FR:"Ouvrir un fichier",L_EN:"Open file"},
      "os_msg_delete_file":{L_FR:"Supprimer le fichier",L_EN:"Delete file"},
      "os_msg_save_db_as":{L_FR:"Enregistrer la BDD sous",L_EN:"Save DB as"}, 
      "os_msg_save_content_as":{L_FR:"Enregistrer le contenu sous",L_EN:"Save content as"},
      "inputboxMagnetDeclinValue":{L_FR:"Valeur à ajouter ( 0 = auto )",L_EN:"Value to add ( 0 = auto )"},
      "msg_download_from":{L_FR:"Télécharger depuis",L_EN:"Download from"},
      "msg_download_try_manually":{L_FR:"Essayer manuellement",L_EN:"Try manually"},
      "msg_version_verify":{L_FR:"Vérifier sur",L_EN:"Verify on"},
      "msg_elevation_json_invalid":{L_FR:"Format de données non valide.",L_EN:"Invalid data format."},
      "msg_declination_json_invalid":{L_FR:"Format de données non valide.",L_EN:"Invalid data format."},
      "dummy":{L_FR:"In French", L_EN:"In English"}
   }
   return mapping[strKey][gLOCALE]

class PopText(tk.Text,tk.Tk): # classe surchargée pour afficher un popup-menu sur un tk.Text
    # source https://stackoverflow.com/questions/12014210/tkinter-app-adding-a-right-click-context-menu
    # Comportement particulier, il faut maintenir le clic-droit pour sélectionner une option du sous-menu
    
    def __init__(self,parentTkWindow):
        tk.Text.__init__(self)
        TAB="  "
        self.parentWindow=parentTkWindow # permet de connaitre la fenetre parent pour l'utilisation du clipboard
        
        self.popup_menu = tk.Menu(self, tearoff=0)   
        self.popup_menu.add_command(label=i18n("PopText_close"), command=self.close)
        self.popup_menu.add_command(label=TAB+i18n("PopText_copy"), command=self.copy)
        self.popup_menu.add_command(label=TAB+i18n("PopText_cut"), command=self.cut)
        self.popup_menu.add_command(label=TAB+i18n("PopText_past"), command=self.past)
        self.popup_menu.add_separator()
        self.popup_menu.add_command(label=TAB+i18n("PopText_select_row"), command=self.select_row)
        self.popup_menu.add_command(label=TAB+i18n("PopText_cut_row"), command=self.cut_row)
        self.popup_menu.add_command(label=TAB+i18n("PopText_insert_row"), command=self.insert_row)
        self.popup_menu.add_separator()
        self.popup_menu.add_command(label=TAB+i18n("PopText_select_all"), command=self.select_all)
        #self.popup_menu.add_separator()
        #self.popup_menu.add_command(label=TAB+i18n("PopText_cancel"), command=self.cancel)
        #self.popup_menu.add_command(label=TAB+i18n("PopText_rows_count"), command=self.rows_count)
        # bind events
        self.bind("<Button-3>", self.popup)  # on right_click - Button-2 on Aqua
        self.bind("<KeyRelease-KP_Enter>",self.kp_enter_release) # <KeyRelease> -KP_Enter
        self.bind("<Alt-c>",self.kp_alt_c)
        self.bind("<Alt-x>",self.kp_alt_x)
        self.bind("<Alt-v>",self.kp_alt_v)
        self.bind("<Alt-l>",self.kp_alt_l)
        self.bind("<Alt-a>",self.kp_alt_a)
        self.bind("<Alt-i>",self.kp_alt_i)
        self.bind("<Alt-z>",self.kp_alt_z)
        self.bind("<Alt-k>",self.kp_alt_k)
        self.bind("<Alt-KP_8>",self.kp_alt_Up)
        self.bind("<Alt-KP_2>",self.kp_alt_Dn)

    def popup(self, event):
        try:
            self.popup_menu.tk_popup(event.x_root, event.y_root, 0)
        finally:
            self.popup_menu.grab_release()
    # --------------------------------------------------------
    def kp_enter_release(self, event):
       # insère une ligne vierge après la position courante
       try:
          self.insert(self.curPos(), "\n")        
       finally:
          pass

    def kp_alt_c(self, event):
       # insère une ligne vierge avant la ligne courante
       try:
          self.copy()
       finally:
          pass

    def kp_alt_x(self, event):
       try:
          self.cut()
       finally:
          pass

    def kp_alt_v(self, event):
       try:
          self.past()
       finally:
          pass

    def kp_alt_l(self, event):
       try:
          self.select_row()
       finally:
          pass

    def kp_alt_a(self, event):
       try:
          self.select_all()
       finally:
          pass

    def kp_alt_i(self, event):
       # insère une ligne vierge avant la ligne courante
       try:
          self.insert_row()
       finally:
          pass

    def kp_alt_z(self, event):
       try:
          self.restore()
       finally:
          pass

    def kp_alt_k(self, event):
       try:
          self.cut_row()
       finally:
          pass

    def kp_alt_Up(self, event):
       # permute la ligne avec la ligne précédente
       try:
          self.move_row_up()
       finally:
          pass

    def kp_alt_Dn(self, event):
       # permute la ligne avec la ligne suivante (si pas la dernière)
       try: 
          self.move_row_dn()
       finally:
          pass

    # --------------------------------------------------------
    def curPos(self):
       # current Cursor Position
       return self.index(tk.INSERT)

    def curPosRowStartEnd(self,offset=0):
       # renvoie start,end de ligne de la sélection
       index=self.curPos()
       strRow=index.split(".")[0]
       intRow=int(strRow)+offset
       strRow=str(intRow)
       start=strRow+".0"
       end=strRow+".end"
       return start,end
    
    def hasSelection(self):
       return self.tag_ranges("sel")

    def displayContent(self,strContent): # affiche la chaine dans le champ dédié
       self.delete("1.0", tk.END)
       self.insert("1.0",strContent)

    def getDisplayContent(self):
       return self.get("1.0", tk.END)

    def getTrimDisplayContentArray(self):
       self.delete_last_blank_rows()
       strContenu=self.getDisplayContent()+"\n"
       return strContenu.split("\n")

    def close(self):
       # ferme le popup-menu
       pass

    def copy(self):
       # copie la sélection dans le clipboard
       if self.hasSelection(): # selection exists
          self.parentWindow.clipboard_clear()  # clear clipboard-contents
          self.parentWindow.clipboard_append(self.selection_get()) # copy selection in clipboard

    def past(self):
       # colle le clipboard-contents à la currentCursorPosition
       self.insert(self.curPos(), self.parentWindow.clipboard_get())
       start,end=self.curPosRowStartEnd()
       self.mark_set(tk.INSERT, start) # movecursor

    def cancel(self):
       # colle le clipboard-contents à la currentCursorPosition
       self.delete("1.0", tk.END)
       self.insert("1.0",self.parentWindow.clipboard_get())

    def cut(self):
       # si la sélection est une chaine, copie la chaine dans le clipboard et coupe la chaine.
       # si la sélection est une ligne vierge, supprime la ligne
       if self.hasSelection(): # si selection existe alors coupe la chaine
          strContent=self.get(tk.SEL_FIRST,tk.SEL_LAST)
          if len(strContent)>0:
             self.copy() 
             self.delete(tk.SEL_FIRST,tk.SEL_LAST)
       else: # la sélection est une ligne vierge, supprime la ligne
          start,end=self.curPosRowStartEnd()
          strRowContent=self.get(start,end)
          if len(strRowContent)==0: 
             self.delete(start, end+"+1c") #self.delete_row()

    def cut_row(self):
       # coupe la ligne
       self.select_row()
       self.cut()

    def select_all(self):
       # sélectionne toutes les lignes
       self.tag_add(tk.SEL, "1.0", tk.END)
       self.mark_set(tk.INSERT, "1.0")
       self.see(tk.INSERT)
       return 'break'

    def insert_row(self):
       # insère une ligne vierge avant la ligne courante
       start,end=self.curPosRowStartEnd()
       self.insert(start, "\n")
       start,end=self.curPosRowStartEnd(-1)
       self.mark_set(tk.INSERT, start) # movecursor

    def delete_row(self):
       # supprime la ligne
       self.delete('current linestart', 'current lineend+1c')
       #return "break" uncomment if you want to disable selection by double-clicking

    def select_row(self):
       # sélectionne le texte de la ligne
       start,end=self.curPosRowStartEnd()
       self.tag_add(tk.SEL,start,end)

    def move_row_UpDn(self,start, end, intOffset):
       strRowContent=self.get(start,end)
       if len(strRowContent)>0:
          self.select_row()
          self.copy()
          self.delete(start, end+"+1c")
          start,end=self.curPosRowStartEnd(intOffset)
          self.insert(start, "\n") # insert row
          self.mark_set(tk.INSERT, start) # movecursor
          self.past()
          self.select_row()

    def move_row_up(self):
       start,end=self.curPosRowStartEnd()
       if int(start.split('.')[0]) > 1:
          self.move_row_UpDn(start, end, -1)

    def move_row_dn(self):
       start,end=self.curPosRowStartEnd()
       if int(start.split('.')[0]) < int(self.index('end-1c').split('.')[0]):
          self.move_row_UpDn(start, end, 1)
 
    def rows_count(self):
       # comptabilise le nbr de lignes
       intNbrLignes=self.index('end-1c').split('.')[0]
       strChaine="Nbr de lignes: "+str(intNbrLignes)
       msg(strChaine)

    def delete_last_blank_rows(self):
      # supprime les lignes vierges en fin
      intNbrLignes=int(self.index('end-1c').split('.')[0])
      if intNbrLignes>1:
         for i in range(intNbrLignes-1, -1, -1):
            strRow=str(i)
            start=strRow+".0"
            end=strRow+".end+1c"
            strContent=self.get(start,end)
            #print(start,end,strContent,str(len(strContent)))
            if len(strContent)==1: 
               self.delete(start,end)
            else:
               break

    def append(self,strString,side="r"):
       if side=="r":
          self.insert(tk.END,strString+"\n")
       else:
          self.insert(tk.END,"\n"+strString)

    def rupt(self):
      # efface le contenu
      #self.select_all()
      #self.cut()
      self.delete("1.0", tk.END)
      # ancien comportement:
      #self.focus_set() # permet de voir le curseur 
      #self.insert(tk.END,"\n"+"\n") # ajoute 2 lignes vierges en fin avant d'afficher le résultat
      #self.tag_add(tk.SEL,"1.0",self.index('end-1c')) # sélectionne toutes les lignes (pour une future suppression)
      #self.mark_set(tk.INSERT, "1.0") # curseur au début
      #self.see(tk.END) # ascenseur

    def backupPath(self): 
      fileName=cmd.scriptBaseName()
      arrFileRootExt=cmd.os.path.splitext(fileName)
      fileName=arrFileRootExt[0]+"_content"+".tmp"
      return fileName

    def backup(self):
       strContent=self.getDisplayContent()
       if len(strContent.strip())>0:
          cheminFichier=self.backupPath()
          objFic = open(cheminFichier, "w")
          objFic = open(cheminFichier, "a")
          objFic.write(strContent)   # sauvegarde le fichier
          objFic.close()

    def restore(self):
       strContent=""
       cheminFichier=self.backupPath()
       if cmd.os.path.exists(cheminFichier):
          objFic = open(cheminFichier, "r")
          strContent=objFic.read()   # charge le fichier 
          objFic.close()
          if len(strContent.strip())>0:
             self.displayContent(strContent)
             self.delete_last_blank_rows()

# -------------------------------------------------------------------------------
def cheminFichierHelp():
   strBasename=cmd.os.path.splitext(argv[0])[0]
   strBasename=strBasename.replace("./","")
   strFilename=strBasename.replace(".py","")  + "_help_" # + gLOCALE.lower()+".txt"
   return strFilename

def setLOCALE(): # set global LOCALE variable to set app language like help_file
   strLang=""
   strReturn=gLOCALE
   arrFiles=cmd.os.listdir('.')
   arrFiles.sort()
   for file in arrFiles:
      strBasename=cmd.os.path.splitext(file)[0]
      if strBasename.find(cheminFichierHelp())!=-1:
         strLang=strBasename[len(cheminFichierHelp()):].upper()
         if strLang in VALID_L: 
            strReturn=strLang
   return strReturn

# -------------------------------------------------------------------------------
def msg(strTexte):
      msgbox.showinfo(moduleCmd, strTexte)

def isNumeric(strValue):
   return re.match('^[0-9\.\-]*$',strValue)

def loopBearing(bearing):
   newBear=bearing
   if newBear>=360: newBear=newBear-360
   if newBear<0: newBear=360+newBear
   return newBear
# -------------------------------------------------------------------------------
def csvCommaIsInString(strString):
   # renvoie True si la chaine contient une virgule
   boolReturn=(strString.find(',')>0)
   return boolReturn

def csvLineToArray(strCsvLine):
   # renvoie un tableau de valeurs de la chaine CSV.
   # renvoie None si la chaine ne contient pas de virgule.
   arrCols=None
   if csvCommaIsInString(strCsvLine): 
      arrCols=re.split(r'(?!\B"[^"]*),(?![^"]*"\B)',strCsvLine,flags=re.IGNORECASE)
   return arrCols

def csvIndexOfCol(strCsvHeader,strSearchColName):
   # renvoie l'index de la colonne recherchée dans l'entête CSV.
   # renvoie None si le nom de la colonne recherchée ne s'y trouve pas.
   p=None
   i=0
   arrCols=csvLineToArray(strCsvHeader)
   strSearchValue=strSearchColName
   if not arrCols is None:
      for strCol in arrCols:
         #if strSearchValue[0:3] in ("lat","lon"):
         if strSearchValue[0:3]!="ele":
            if strCol[0:3]==strSearchValue[0:3]: # prend que la partie gauche pour palier à lat/latitude et lon/longitude
               p=i
         else:
            if strCol==strSearchValue:
               p=i
         i=i+1
   return p

def csvIsGpxHeader(strHeader,strSearchTerm=""):
   # renvoie True si trouve la chaine "lat,lon" dans strHeader,
   # si strSearchTerm est spécifié, recherchera, en plus, la présence de ce terme dans strHeader
   # le terme Gpx dans le nom de la fonction fait référence aux champs obligatoires du format GPX 
   boolReturn=False
   if not strHeader.find('lat,lon')==-1 or not strHeader.find('latitude,lon')==-1:
      if strSearchTerm=="":
         boolReturn=True
      else:
         if not strHeader.find(strSearchTerm+",")==-1 or not strHeader.find(","+strSearchTerm)==-1 :
            boolReturn=True
   return boolReturn

def csvIsGpxRowOfValues(strLine):
   # renvoie True si strLine ne contient pas d'entêtes et donc, 
   # dans ce cas, strLine contient des valeurs séparées par une virgule
   boolReturn=False
   if csvCommaIsInString(strLine) and not csvIsGpxHeader(strLine):
      boolReturn=True
   return boolReturn

def csvJoinStdValues(strType,lat,lon,strSym,strName):
   strCSV=strType+","+str(lat)+","+str(lon)+","+strSym+","+strName
   return strCSV

def csvJoinValues(strType,lat,lon,strSym,strName,dist,bearing,strElevation="",strEleFt=""):
   strEle=""
   if strElevation!="" and strEleFt!="": strEle=strElevation+","+strEleFt+","
   strCSV=csvJoinStdValues(strType,lat,lon,strSym,strName)+","+strEle+str("%.2f"%dist)+","+str("%.2f"%bearing)
   return strCSV

def csvJoinStrings(strType,strLat,strLon,strSym,strName,strDist="",strBearing=""):
   if len(strDist)>0: strDist=","+strDist
   if len(strBearing)>0: strBearing=","+strBearing
   strCSV=strType+","+strLat+","+strLon+","+strSym+","+strName + strDist + strBearing
   return strCSV
# -------------------------------------------------------------------------------
def distNmBetween2Points(floatLat1,floatLon1,floatLat2,floatLon2):
   lat1 = floatLat1 * math.pi/180
   lat2 = floatLat2 * math.pi/180
   dlon = (floatLon2-floatLon1) * math.pi/180
   R = 6371010/1852
   dNm = math.acos(math.sin(lat1)*math.sin(lat2) + math.cos(lat1)*math.cos(lat2) * math.cos(dlon)) * R;
   return dNm

def distNmBetween2PointsBis(floatLat1,floatLon1,floatLat2,floatLon2):
   # autre methode de calcul
   distance=0
   if floatLat1==floatLat2:
      distance=60 * math.fabs(floatLon1-floatLon2) * math.cos(math.radians(floatLat1))
   else:
      tempo=math.atan( math.fabs(floatLon1-floatLon2) / math.fabs(floatLat1-floatLat2) * math.cos(math.radians(math.fabs(floatLat1+floatLat2))/2) )
      distance=math.fabs(floatLat1-floatLat2) / math.cos(tempo) *60
   return distance

def bearingBetween2Points(floatLat1,floatLon1,floatLat2,floatLon2):
   lat1=math.radians(floatLat1)
   lon1=math.radians(floatLon1)
   lat2=math.radians(floatLat2)
   lon2=math.radians(floatLon2)
   dLon=lon2-lon1
   Y=math.sin(dLon) * math.cos(lat2)
   X=math.cos(lat1)*math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dLon)
   bearing=math.degrees(math.atan2(Y,X))
   if bearing<0: bearing=bearing+360
   return bearing


def pointRadialDistance(lat1, lon1, bearing, distance):
    """
    Return final coordinates (lat2,lon2) [in degrees] given initial coordinates
    (lat1,lon1) [in degrees] and a bearing [in degrees] and distance [in NM]
    """
    rEarth = 6371.010 # Earth's average radius in km
    epsilon = 0.000001 # threshold for floating-point equality

    rlat1 = math.radians(lat1)
    rlon1 = math.radians(lon1)
    rbearing = math.radians(bearing)
    rdistance = distance *1.852 / rEarth # normalize linear distance to radian angle  / 1.852 

    rlat2 = math.asin( math.sin(rlat1) * math.cos(rdistance) + math.cos(rlat1) * math.sin(rdistance) * math.cos(rbearing) )

    #if math.cos(rlat1) == 0 or math.fabs(math.cos(rlat1)) < epsilon: # Endpoint a pole
    #    rlon2=rlon1
    #else:
    #    rlon2 = ((rlon1 - math.asin( math.sin(rbearing)* math.sin(rdistance) / math.cos(rlat1) ) + math.pi ) % (2*math.pi) ) - math.pi

    if ((math.cos(rlat2) == 0) and (math.fabs(math.cos(rlat2)) < epsilon)):
       rlon2 = rlon1;
    else:
       rlon2 = rlon1 + math.atan2(math.sin(rbearing) * math.sin(rdistance) * math.cos(rlat1), math.cos(rdistance) - math.sin(rlat1) * math.sin(rlat2));

    lat = math.degrees(rlat2)
    lon = math.degrees(rlon2)
    return (lat, lon)

def cheminFichier(): # renvoie le nom du script "ligne de commandes" sans l'extension. exemple: "parcours"
   fileName=cmd.scriptBaseName()
   arrFileRootExt=cmd.os.path.splitext(fileName)
   fileName=arrFileRootExt[0] 
   return fileName

def cheminFichierTexte():
   return cheminFichier()+".txt"

def cheminFichierCSV():
   return cheminFichier()+".csv"

def cheminFichierGPX():
   return cheminFichier()+".gpx"

def cheminFichierXML():
   return cheminFichier()+".xml"

def cheminFichierLOG():
   return cheminFichier()+".log"

def cheminFichierConfig():
   return cheminFichier()+".cfg"

def cheminFichierInterCsv():
   return cheminFichier()+"_inter.csv"

def cheminFichierDB(): # renvoie le nom du fichier sélectionné et stocké dans le champ dédié
   fichier=lblSourceFilename['text']
   return fichier

def setContenuFichier(cheminFichier,strContenu): # fonction générique, écrit le contenu dans le fichier
   objFic = open(cheminFichier, "w")
   objFic = open(cheminFichier, "a")
   objFic.write(strContenu.rstrip("\n"))   # sauvegarde le fichier
   objFic.close()


def setContenuFichierTexte(strContenu): # écrit le contenu dans le fichier texte
   fichier=cheminFichierTexte()
   setContenuFichier(fichier,strContenu)


def setContenuFichierConfig(strContenu): # écrit le contenu dans le fichier de config
   fichier=cheminFichierConfig()
   setContenuFichier(fichier,strContenu)


def getContenuFichier(strCheminFichier): # fonction générique, renvoie le contenu du fichier
   strReturn=""
   if strCheminFichier!="":
      if cmd.os.path.exists(strCheminFichier):
         objFic = open(strCheminFichier, "r")
         strReturn=objFic.read()   # charge le fichier 
         objFic.close()
   return strReturn


def getContenuFichierTexte(): # renvoie le contenu du fichier texte
   strReturn=getContenuFichier(cheminFichierTexte())
   return strReturn 


def getContenuFichierCSV(): # renvoie le contenu du fichier CSV
   strReturn=getContenuFichier(cheminFichierCSV())
   return strReturn


def getContenuFichierGPX(): # renvoie le contenu du fichier GPX
   strReturn=getContenuFichier(cheminFichierGPX())
   return strReturn

def getContenuFichierXML(): # renvoie le contenu du fichier XML
   strReturn=getContenuFichier(cheminFichierXML())
   return strReturn


def getContenuFichierLOG(): # renvoie le contenu du fichier LOG
   strReturn=getContenuFichier(cheminFichierLOG())
   return strReturn


def getContenuFichierConfig():  # renvoie le contenu du fichier Config
   strReturn=getContenuFichier(cheminFichierConfig())
   return strReturn


def getContenuFichierDB():  # renvoie le contenu du fichier DB
   strReturn=getContenuFichier(cheminFichierDB())
   return strReturn

def getContenuFichierInterCsv():  # renvoie le contenu du fichier Inter Csv
   strReturn=getContenuFichier(cheminFichierInterCsv())
   return strReturn

def setCurrentFichierDB(strFileName): # affiche le nom de fichier sélectionné dans le champ dédié
   lblSourceFilename['text']=strFileName

def lstFichiersOnSelect(event): # évènement de sélection d'un elt dans la liste des fichiers
   if event.widget.curselection():
      setCurrentFichierDB(event.widget.get(event.widget.curselection()))

def lstFichiersInit(strSelectedFile=""): # initialise la liste des fichiers
   arrFiles=cmd.os.listdir('.')
   arrExt=('.dat','.gpx')
   i=0
   s=0
   arrFiles.sort()
   for file in arrFiles:
      if cmd.os.path.splitext(file)[1] in arrExt:
         lstFichiers.insert(i, file)
         if strSelectedFile!="":
            strSelectedFile=strSelectedFile.replace("./","")
            if strSelectedFile == file: s=i
         i=i+1
   #lstFichiers.config(yscrollcommand=scrollVFichiers.set)
   lstFichiers.select_set(s)
   lstFichiers.event_generate("<<ListboxSelect>>")

def lstFichiersRefresh(strSelectedFile=""): # rafrachit la liste des fichiers
   lstFichiers.delete(0,tk.END)
   lstFichiersInit(strSelectedFile)

def pleaseWaitOn():
   lblInfoLib2['text']=i18n("lblInfoLib2")+"..."
   lblInfoLib2.master.update()
   #msgbox.showinfo('Info',lblInfoLib2['text'])

def pleaseWaitOff():
   lblInfoLib2['text']=""

def rbtnTypeOnChange():
   # si le contenu est en CSV, change le type de point pour chaque ligne du CSV en fonction du choix utilisateur 
   strNewType=strRbtnTypeId.get()

   strFoundType="T"
   if strNewType=="T": strFoundType="W"
   strNewType=strNewType+","
   strFoundType=strFoundType+","

   strContenu=txtDisplayContent.getDisplayContent()+"\n"
   arrContenu=strContenu.split("\n")
   strContenu=""

   i=0
   for strLine in arrContenu:
      i=i+1
      if len(strLine)>2: 
         if strLine[0:2]==strFoundType:
            txtDisplayContent.delete(str(i)+".0",str(i)+".2")
            txtDisplayContent.insert(str(i)+".0",strNewType)

#-----------------------------------
def getTabIndex():
   i=0
   #strTabName=nbkMain.tab(nbkMain.select(),"text")
   #if strTabName=="Sandbox":i=1
   if nbkMain.index('current')==1: i=1
   return i

#-----------------------------------
# fonctions de la barre de menus
#-----------------------------------
def mnuAppShowConfigOnClick():
   strContent=getContenuFichierConfig()
   if strContent=="":
      strContent=cmd.getDefaultConfigContent()
      setContenuFichierConfig(strContent)
      strContent=getContenuFichierConfig() # recharge le fic pour s'assurer de la bonne prise en compte
   txtDisplayContent.displayContent(strContent)

def mnuAppSaveConfigOnClick():
   txtDisplayContent.backup()
   strContent=txtDisplayContent.getDisplayContent()
   setContenuFichierConfig(strContent)

def mnuAppResetConfigOnClick():
   strContent=cmd.getDefaultConfigContent()
   txtDisplayContent.displayContent(strContent)

def mnuAppOpenFileOnClick():
   arrTypesFiles=[('csv file','.csv'),('gpx file','.gpx'),('data file','.dat'),('xml file','.xml'),('txt file','.txt'),('json file','.json')]
   strFilename=tkFileDialog.askopenfilename(title=i18n("os_msg_open_file"),filetypes=arrTypesFiles)
   if strFilename:
      strContenu=getContenuFichier(strFilename)
      txtDisplayContent.displayContent(strContenu)

def mnuAppSaveContentAsOnClick():
   arrTypesFiles=[('all files','.*'),('csv file','.csv'),('gpx file','.gpx'),('data file','.dat'),('xml file','.xml'),('txt file','.txt')]
   #strDefaultExt=".gpx"
   strFilename=tkFileDialog.asksaveasfilename(title=i18n("os_msg_save_content_as"),filetypes=arrTypesFiles)
   if strFilename:
      strContenu=txtDisplayContent.getDisplayContent()
      if cmd.sys.version_info.major<3: strContenu=strContenu.encode("utf-8")
      setContenuFichier(strFilename,strContenu)
      strFilename=cmd.os.path.split(strFilename)[1]
      lstFichiersRefresh(strFilename)

def mnuAppHelpOnClick():
   strFilename=cheminFichierHelp() + gLOCALE.lower() + ".txt"
   if cmd.os.path.exists(strFilename):
      strContenu=getContenuFichier(strFilename)
      txtDisplayContent.displayContent(strContenu)
   else:
      msg(i18n("mnuAppHelp_msg_filenotfound") + "\n"+strFilename )

def mnuAppAboutOnClick():
   #msg("Version: "+str(SCRIPT_VERSION))
   strUrl="" # REPOSITORY_URL+"/version.json"
   TIMEOUT=10
   boolError=False
   strContent="URL: "+REPOSITORY_URL+"\n"
   distantVersion=0
   distantGuiVersion=0
   distantCmdVersion=0
   strGuiFile=moduleCmd+"_ui.py"
   strCmdFile=moduleCmd+".py"
   arrResults=[]
   i=0
   p=0
   strMsgTitle="Version"
   strSearchTerm="SCRIPT_VERSION"
   strUrl1=DOWNLOAD_URL%strGuiFile
   strUrl2=DOWNLOAD_URL%strCmdFile

   if msgbox.askyesno(strMsgTitle,strMsgTitle+": "+str(SCRIPT_VERSION)+"\n"+i18n("msg_version_verify")+":\n"+REPOSITORY_URL+" ?"): 
      txtDisplayContent.displayContent(strContent)
      pleaseWaitOn()
      try:
         request1=urllib2.Request(strUrl1,None)
         result1=urllib2.urlopen(request1,timeout=TIMEOUT)
         request2=urllib2.Request(strUrl2,None)
         result2=urllib2.urlopen(request2,timeout=TIMEOUT)
      except IOError as e:
         boolError=True
         pleaseWaitOff()
         msg("Warning. "+str(e)+"\n")

      else:
         arrResults.append(result1.read().decode("utf-8")) # result.read() est un tableau de bytes
         arrResults.append(result2.read().decode("utf-8"))
         pleaseWaitOff()
         for strResult in arrResults:
            i=i+1  
            arrContent=strResult.split("\n")
            p=len(strSearchTerm)
            for strLine in arrContent: # boucle sur chaque ligne du fichier téléchargé
               if strLine[0:p]==strSearchTerm: # si trouve le num_version...
                  strLine=strLine[p+1:]
                  distantVersion=float(strLine)
                  if i==1: distantGuiVersion=distantVersion
                  if i==2: distantCmdVersion=distantVersion
                  break

         if distantGuiVersion>0 and distantCmdVersion>0: 
            strMsg=("(GUI) "+strGuiFile+"\n"+
                  "Local  : "+str(SCRIPT_VERSION)+"\n"+
                  "Distant:"+str(distantGuiVersion)+"\n\n"+
                  "(CMD) "+strCmdFile+"\n"+
                  "Local  : "+str(cmd.SCRIPT_VERSION)+"\n"+
                  "Distant: "+str(distantCmdVersion)
            )
            txtDisplayContent.displayContent(strContent+strMsg)
            if SCRIPT_VERSION==distantGuiVersion and cmd.SCRIPT_VERSION==distantCmdVersion:
               msgbox.showinfo(strMsgTitle,"Local version = Distant version")
            else:
               msgbox.showinfo(strMsgTitle,strMsg)
         else:
               msgbox.showinfo(strMsgTitle,strMsgTitle+" can not be verified.") # ça ne devrait pas arriver


def mnuDataShowDBFileOnClick():
   txtDisplayContent.backup()
   strContent=getContenuFichierDB()
   txtDisplayContent.displayContent(strContent)

def mnuDataExtractDBFileOnClick():
   if txtDisplayContent.search("<extract>","1.0",stopindex=tk.END):
      strElementTreeEncoding=cmd.xmlEltTreeEncoding()
      strFileName=cheminFichierDB()
      pleaseWaitOn()
      strNewFileName=cmd.extractFromGpxFile(strFileName,strElementTreeEncoding)
      pleaseWaitOff()
      if strNewFileName!="":
         # refresh liste fichiers
         lstFichiersRefresh(strNewFileName)
         mnuDataShowDBFileOnClick()
   else:
      msgbox.showinfo('Info',i18n("mnuDataExtractDBFile_msg_info_extract"))

def mnuDataConvertDBFileToCSVOnClick():
      strElementTreeEncoding=cmd.xmlEltTreeEncoding()
      strFileName=cheminFichierDB()
      strPtType=strRbtnTypeId.get()
      pleaseWaitOn()
      strFileName=cmd.convertGpxFileToCsvFile(strFileName,strPtType,strElementTreeEncoding)
      pleaseWaitOff()
      if strFileName!="":
         txtDisplayContent.backup()
         strContent=getContenuFichier(strFileName)
         txtDisplayContent.displayContent(strContent)

def mnuDataRefreshDBFileListOnClick():
   lstFichiersRefresh()

def mnuDataSaveAsDBFileListOnClick():
   arrTypesFiles=[('all files','.*'),('csv file','.csv'),('gpx file','.gpx'),('data file','.dat'),('xml file','.xml'),('txt file','.txt')]
   #strDefaultExt=".gpx"
   strFilename=tkFileDialog.asksaveasfilename(title=i18n("os_msg_save_db_as"),filetypes=arrTypesFiles,initialfile=cheminFichierDB())
   if strFilename:
      strContenu=getContenuFichier(cheminFichierDB())
      setContenuFichier(strFilename,strContenu)
      strFilename=cmd.os.path.split(strFilename)[1]
      lstFichiersRefresh(strFilename)

def mnuDataDeleteDBFileOnClick():
   strFileName=cheminFichierDB()
   if msgbox.askyesno("Question",i18n("os_msg_delete_file")+" %s ?"%strFileName):
      if cmd.os.path.exists(strFileName):
         cmd.os.remove(strFileName)
         lstFichiersRefresh()
         txtDisplayContent.displayContent(" ")


def mnuSearchSearchOnClick():
    #msgbox.showinfo('Info', 'Effacement du champ résultat')
    txtDisplayContent.delete("1.0", tk.END)

    #msgbox.showinfo('Info', 'Enregistrement du fichier texte')
    strContent=txtContenuFichierTexte.get("1.0", tk.END)
    setContenuFichierTexte(strContent)

    #msgbox.showinfo('Info', 'Recherche en cours...')
    pleaseWaitOn()
    strType=strRbtnTypeId.get()
    strCheminFichierSource=cheminFichierDB()
    cmd.main(strCheminFichierSource,strType,strTxtInputFileName=cheminFichierTexte())
    pleaseWaitOff()

    #msgbox.showinfo('Info', 'Afficher le résultat')
    txtDisplayContent.backup()
    strContent=getContenuFichierCSV()
    txtDisplayContent.displayContent(strContent)

def mnuSearchShowCSVOnClick():
   txtDisplayContent.backup()
   strContent=getContenuFichierCSV()
   txtDisplayContent.displayContent(strContent)

def mnuSearchShowGPXOnClick():
   txtDisplayContent.backup()
   strContent=getContenuFichierGPX()
   txtDisplayContent.displayContent(strContent)

def mnuSearchShowXMLOnClick():
   txtDisplayContent.backup()
   strContent=getContenuFichierXML()
   txtDisplayContent.displayContent(strContent)

def mnuSearchShowLogOnClick():
   txtDisplayContent.backup()
   strContent=getContenuFichierLOG()
   txtDisplayContent.displayContent(strContent)


def mnuToolsDist2PointsOnClick():
   # calcule la distance entre deux points consécutifs
   strEntetesCSV=""
   colLat=0
   colLon=0
   i=0
   p=0

   floatPreviousLat=0.0
   floatPreviousLon=0.0
   floatLat=0.0
   floatLon=0.0

   floatDistance=0.0
   floatBearing=0.0
   boolPrevious=False
   strType="T,"
   strInterPoints="type,latitude,longitude"
   arrInterPoints=[] # strInterPoints
   strInterPoint=""
   floatInterLat=0.0
   floatInterLon=0.0
   floatInterDist=0
   d=0

   intLOL=0
   strEOL='1.end'

   NEWCOLS="dist,bearing"   

   arrContenu=txtDisplayContent.getTrimDisplayContentArray()

   # recherche la position des colonnes lat et lon
   if len(arrContenu[0])>0:
      strLine=arrContenu[0].strip()
      if csvIsGpxHeader(strLine) and not (csvIsGpxHeader(strLine,"dist") or csvIsGpxHeader(strLine,"bearing")):
         colLat=csvIndexOfCol(strLine,"lat")
         colLon=csvIndexOfCol(strLine,"lon")

   if colLon==0: # avertissement si la col Lon n'a pas été trouvée
      msg(i18n("mnuToolsDist2Points_msg_info_err"))
   else:
      txtDisplayContent.backup()
      txtDisplayContent.rupt()
      for strLine in arrContenu:
         i=i+1
         strLine=strLine.strip()
         intLOL=len(strLine) # longueur de la ligne
         if intLOL>0:
            strValue=""
            strEOL=str(i)+'.end' # ou alors str(i)+"."+str(intLOL) pour la posi de fin de ligne
            
            if csvIsGpxHeader(strLine): # si les noms de colonnes obligatoires se trouvent dans la ligne...
               strValue=NEWCOLS
               boolPrevious=False

            else: 
               arrValues=csvLineToArray(strLine)
               floatLat=float(arrValues[colLat])
               floatLon=float(arrValues[colLon])
               
               if boolPrevious:
                  # calculs distance et bearing
                  floatDistance=distNmBetween2Points(floatPreviousLat,floatPreviousLon,floatLat,floatLon) 
                  floatBearing=bearingBetween2Points(floatPreviousLat,floatPreviousLon,floatLat,floatLon)

                  # points intermediaires
                  arrInterPoints.append(strInterPoints)
                  floatInterDist=0 #floatDistance-int(floatDistance)
                  while floatInterDist <= floatDistance:
                     (floatInterLat,floatInterLon) = pointRadialDistance(floatPreviousLat,floatPreviousLon,floatBearing,floatInterDist)
                     strInterPoint=strType+str(floatInterLat) + "," + str(floatInterLon) # "," + str("%.2f"%floatInterDist)+","+str("%.2f"%floatBearing)
                     floatInterDist=floatInterDist+1 # 1 NM
                     arrInterPoints.append(strInterPoint)

               else:
                  # pas de calcul, valeurs à 0
                  floatDistance=0.0
                  floatBearing=0.0

               boolPrevious=True
               floatPreviousLat=floatLat
               floatPreviousLon=floatLon

               # concat les valeurs dans la chaine à afficher
               strValue=str("%.2f"%floatDistance)+","+str("%.2f"%floatBearing)
            
            # ajoute la chaine en fin de ligne
            #txtDisplayContent.insert(strEOL,","+strValue)

            txtDisplayContent.append(strLine+","+strValue)

      txtDisplayContent.delete_last_blank_rows()

      if len(arrInterPoints)>0: 
         setContenuFichier(cheminFichierInterCsv(),"\n".join(arrInterPoints))
         #for strInterPoint in arrInterPoints:
         #   print(strInterPoint)

def mnuToolsIntermediatePtOnClick():
   strContent=getContenuFichierInterCsv()
   if len(strContent)>0:
      txtDisplayContent.backup()
      txtDisplayContent.displayContent(strContent)
   else:
      msg("Error")


def mnuToolsCoordByBearingDistOnClick():
   # calcule les coordonnées d'un point à partir d'un point existant, d'une distance et d'un bearing
   # Calculating coordinates given a bearing and a distance
   # fonction locale (closure) de concaténation de variables
   def _valuesToCsv(strType,floatLat,floatLon,strSym,strName,floatDist,floatBear):
      strSym2=""
      strName2=""
      if len(strSym)>0:strSym2=","+strSym
      if len(strName)>0:strName2=","+strName
      strResult=strType + str(floatLat) + "," + str(floatLon) + strSym2 + strName2 + "," + str(floatDist) + "," + str(floatBear)
      return strResult

   l=0
   i=0
   n=0
   p=0
   d=0
   boolContinue=True
   boolTrouve=False
   iType=None
   iLat=0
   iLon=1
   iSym=None
   iName=None
   iDist=2
   iBear=3
   dist=0
   bear=0
   strType=""
   strSym=""
   strName=""
   strResult=""

   strDist=""
   strInterval=""
   intInterval=0
   strEle=""
# soit il n'y a pas l'entête CSV et dans ce cas on attent uniquement les valeurs sur la première ligne:
#   lat,lon,dist,bearing
# soit il y a les entêtes et on attend deux lignes ordonnées ainsi avec les valeurs sur la 2ème ligne:
#   lat,lon,dist,bearing
#   -21.31,55.41,15.00,270.00
# ou
#   type,latitude,longitude,sym,name,dist,bearing
#   T,-21.31,55.41,"Airport",[FMEP],15.00,270.00


   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   if len(arrContenu)>0:
      strLine=arrContenu[0]

      if csvIsGpxHeader(strLine):
         if csvIsGpxHeader(strLine,"dist") and csvIsGpxHeader(strLine,"bearing"): 
            l=1 # il y a une ligne d'entêtes avec les noms de colonnes obligatoires
            iType=csvIndexOfCol(strLine,"type")
            iLat=csvIndexOfCol(strLine,"lat")
            iLon=csvIndexOfCol(strLine,"lon")
            iSym=csvIndexOfCol(strLine,"sym")
            iName=csvIndexOfCol(strLine,"name")
            iDist=csvIndexOfCol(strLine,"dist")
            iBear=csvIndexOfCol(strLine,"bearing")
            #txtDisplayContent.append("")
         else:
            boolContinue=False
      else:
         if strLine[0:1] in ('W','T','<') or len(strLine)<3: boolContinue=False # conditions d'arrêt

      if boolContinue:
         txtDisplayContent.backup()
         txtDisplayContent.rupt()
         for strLine in arrContenu:
            strResult=""
            if len(arrContenu)>l: 
               strLine=arrContenu[i].strip()
               if csvIsGpxRowOfValues(strLine):
                  arrColValues=csvLineToArray(strLine)
                  p=p+1
                  if not iType==None:strType=arrColValues[iType]+","
                  if not iLat==None: lat1=float(arrColValues[iLat])
                  if not iLon==None: lon1=float(arrColValues[iLon])
                  if not iSym==None: strSym='"Waypoint"'
                  if not iName==None: strName='Point'+str(p)
                  if not iBear==None: bear=float(arrColValues[iBear])
                  if not iDist==None: 
                     # fonction bonus:
                     # affiche tous les points intermédiaires en fonction de la distance (pas) mentionné sur le 4ème chiffre après la virgule flottante
                     strDist=arrColValues[iDist]
                     strInterval=arrColValues[iDist]
                     d=strInterval.find(".")
                     if d>0:
                         strInterval=strInterval[d+1:]
                         if len(strInterval)>3:
                            strInterval=strInterval[3:4]
                            intInterval=int(strInterval)
                            arrDist=strDist.split(".")
                            strDist=arrDist[0]+"."+arrDist[1][0:3]
                            dist=float("0."+arrDist[1][0:3])
                            d=0 
                            while dist < float(strDist):
                               d=d+1
                               (lat,lon) = pointRadialDistance(lat1,lon1,bear,dist)

                               interName=strName + "-i" + str(d)
                               strResult=_valuesToCsv(strType,lat,lon,strSym,interName,dist,bear) 
                               
                               txtDisplayContent.append(strResult)
                               dist=dist+intInterval
                     
                     dist=float(strDist) # 

                  (lat,lon) = pointRadialDistance(lat1,lon1,bear,dist)
                  # --- affiche le résultat sur la dernière ligne
                  boolTrouve=True
                  # concatène en fonction des champs présents (on utilisera donc pas la fonction globale csvJoinValues)
                  strResult=_valuesToCsv(strType,lat,lon,strSym,strName,dist,bear)
                  # affiche le résultat
                  txtDisplayContent.append(strResult)

               else:
                  txtDisplayContent.append(strLine)
            i=i+1
         txtDisplayContent.delete_last_blank_rows()

   if not boolTrouve: msg(i18n("mnuToolsCoordByBearingDist_msg_info_err"))


def mnuToolsMagnetDeclinOnClick():
   # demande à l'utilisateur une valeur de bearing puis si le champ bearing existe dans l'entête CSV, 
   # remplace le bearing de chaque ligne par cette valeur
   # accepte les paramètres o,w,e,-,+ pour le choix du signe (W=plus E=moins)
   # accepte le paramètre m pour calculer les coordonnées du point magnétique correspondant
   # accepte le paramètre t pour calculer la tangente du segment
   # 0 pour une recherche automatique de la déclinaison sur internet
   def getDeclinFromInet(floatLat,floatLon):
      TIMEOUT=10
      now = cmd.datetime.now()
      month = now.month
      result=""
      strDeclin="0"
      strValues="lat1=" + str(floatLat) + "&lon1=" + str(floatLon) + "&resultFormat=json" + "&month=" + str(month)
      strUrl=MAGNETURL_QRY + "?" + strValues
      pleaseWaitOn()
      try:
         request=urllib2.Request(strUrl,None)
         result=urllib2.urlopen(request,timeout=TIMEOUT) 
      except IOError as e: 
         pleaseWaitOff()
         print("Warning. "+str(e)+"\n"+strUrl)
      else:
         strResult=result.read().decode("utf-8") # result.read() est un tableau de bytes
         pleaseWaitOff()
         jsonTree=json.loads(strResult)
         strDeclin=jsonTree["result"][0]["declination"]
      return float(strDeclin)

   strInputOffsetBearing=""
   bearingOffset=0
   l=0
   c=0
   t=0
   boolMagn=False
   boolTan=False
   strEntetes=""
   strPtSym='"Waypoint"'
   arrTan=[90,-90]
   floatDeclin=0
   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   if len(arrContenu)>0:
      if not csvIsGpxHeader(arrContenu[0],"bearing"): # recherche le terme dans la première ligne du tableau
         msg(i18n("mnuToolsMagnetDeclin_msg_info_err"))
      else:
         # demande à l'utilisateur la valeur du OffSet
         strInputOffsetBearing=inputbox.askstring(
            i18n("mnuToolsMagnetDeclin"), 
            prompt=i18n("inputboxMagnetDeclinValue")+":",
            initialvalue=0)

         if not strInputOffsetBearing is None:
            strInputOffsetBearing=strInputOffsetBearing.upper()
            if not (strInputOffsetBearing.find("M")==-1): boolMagn=True
            if not (strInputOffsetBearing.find("T")==-1): boolTan=True

            if not (strInputOffsetBearing.find("E")==-1): 
               strInputOffsetBearing=strInputOffsetBearing.replace("E","")
               strInputOffsetBearing=strInputOffsetBearing.replace("-","")
               strInputOffsetBearing="-"+strInputOffsetBearing

            strInputOffsetBearing=strInputOffsetBearing.replace(" ","")
            strInputOffsetBearing=strInputOffsetBearing.replace("+","")
            strInputOffsetBearing=strInputOffsetBearing.replace("O","")
            strInputOffsetBearing=strInputOffsetBearing.replace("W","")
            strInputOffsetBearing=strInputOffsetBearing.replace("T","")

            if isNumeric(strInputOffsetBearing):
               bearingOffset=float(strInputOffsetBearing)
            else:
               bearingOffset=999 # sera ainsi hors plage

            if bearingOffset>-360 and bearingOffset<360:
               txtDisplayContent.backup()
               for strLine in arrContenu:
                  strLine=strLine.strip()
                  boolIsHeader=False
                  boolIsData=False
                  l=l+1
                  if l==1: 
                     txtDisplayContent.rupt()
                     
                  if csvIsGpxHeader(strLine,"bearing"): # il faut le bearing pour le recalculer avec la variation magnétique 
                     strEntetes=strLine
                     #strResult=strLine
                     strResult="type,latitude,longitude,sym,name"
                     if csvIsGpxHeader(strLine,"elevation"):strResult=strResult+",elevation"
                     if csvIsGpxHeader(strLine,"ele_ft"):strResult=strResult+",ele_ft"
                     strResult=strResult+",dist,bearing"
                  else:
                     if csvCommaIsInString(strLine):
                        arrValues=csvLineToArray(strLine)

                        c=csvIndexOfCol(strEntetes,"type")
                        type1=arrValues[c]

                        c=csvIndexOfCol(strEntetes,"lat")
                        lat1=float(arrValues[c])

                        c=csvIndexOfCol(strEntetes,"lon")
                        lon1=float(arrValues[c])

                        c=csvIndexOfCol(strEntetes,"sym")
                        if c==None:
                           sym1="Waypoint"
                        else:
                           sym1=arrValues[c]

                        c=csvIndexOfCol(strEntetes,"name")
                        if c==None:
                           name1="Point"
                        else:
                           name1=arrValues[c]
                        if name1[-2:]!="-V":name1=name1+"-V" # ajoute un car pour identifier le pt "variant"

                        c=csvIndexOfCol(strEntetes,"elevation")
                        if c==None:
                           elevation=""
                        else:
                           elevation=arrValues[c]

                        c=csvIndexOfCol(strEntetes,"ele_ft")
                        if c==None:
                           ele_ft=""
                        else:
                           ele_ft=arrValues[c]

                        c=csvIndexOfCol(strEntetes,"dist")
                        dist1=float(arrValues[c])

                        c=csvIndexOfCol(strEntetes,"bearing") 
                        bear1=float(arrValues[c])

                        if bearingOffset==0: 
                           floatDeclin=getDeclinFromInet(lat1,lon1) # recherche la déclinaison sur internet
                           bearingOffset= -1 * floatDeclin 

                        newBear=loopBearing(float(bear1) + bearingOffset)
                        
                        # le même point "géographique" avec le nouveau bearing
                        strPtGM=csvJoinValues(type1,lat1,lon1,sym1,name1,dist1,newBear,elevation,ele_ft)

                        # csv final à afficher
                        strResult=strPtGM

                        # calcul de tangente ( = 2 demi-tangentes ) perpendiculaire à la radiale
                        if boolTan: 
                           symTan='"Dot, White"'
                           t=0
                           lenTan=dist1*math.sin(math.radians(5)) # longueur de la demi-tangente
                           if lenTan<1: lenTan=1 # NM

                           for bearTanOffset in arrTan: # 2 valeurs: 90 et -90
                              t=t+1
                              bearTan=bear1+bearTanOffset
                              nameTan=name1+"-T"

                              # le point calculé à l'extrémité de la demi-tangente
                              (latTan,lonTan)=pointRadialDistance(lat1,lon1,bearTan,lenTan)

                              # ajoute le csv au résultat final
                              strPtTan=csvJoinValues("T",latTan,lonTan,symTan,(nameTan+str(t)),lenTan,loopBearing(bearTan+bearingOffset))

                              if t==1: strResult=strResult + "\n" + strEntetes + "\n"
                              strResult=strResult + strPtTan + "\n"


                     else:
                        strResult=strLine

                  # affiche le résultat sur la dernière ligne
                  txtDisplayContent.append(strResult)

               txtDisplayContent.delete_last_blank_rows()

def mnuToolsConvertCsvToGpxOnClick():
   strResult=""
   strEntetes=""
   boolTrackpoints=""

   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   if len(arrContenu)>0:
      strLine=arrContenu[0]
      if csvIsGpxHeader(strLine):
         strEntetes=strLine
         iType=csvIndexOfCol(strLine,"type")
         iLat=csvIndexOfCol(strLine,"lat")
         iLon=csvIndexOfCol(strLine,"lon")
         iSym=csvIndexOfCol(strLine,"sym")
         iName=csvIndexOfCol(strLine,"name")
         iDist=csvIndexOfCol(strLine,"dist")
         iBear=csvIndexOfCol(strLine,"bearing")
         iEle=csvIndexOfCol(strLine,"elevation") # uniquement en mètre dans un gpx
         iEleFt=csvIndexOfCol(strLine,"ele_ft")
      if strEntetes!="":
          for strLine in arrContenu:
            #if csvCommaIsInString(strLine):
            if csvIsGpxRowOfValues(strLine):
               arrColValues=csvLineToArray(strLine)
               strType="wpt"
               strLat="0.0"
               strLon="0.0"
               strSym="Waypoint"
               strName="Point"
               strDist=""
               strBear=""
               strEle=""
               strEleFt=""
               if not iType==None:  
                  if arrColValues[iType]=="T": strType="trkpt"
               if not iLat==None: 
                  strLat=arrColValues[iLat]
               if not iLon==None: 
                  strLon=arrColValues[iLon]
               if not iSym==None: 
                  strSym=arrColValues[iSym]
               if not iName==None: 
                  strName=arrColValues[iName]
               if not iDist==None: 
                  strDist=arrColValues[iDist]
               if not iBear==None: 
                  strBear=arrColValues[iBear]
               if not iEle==None: 
                  strEle=arrColValues[iEle]
               if not iEleFt==None: 
                  strEleFt=arrColValues[iEleFt]
               
               strGpxRow='<' + strType + ' lat="'+strLat+'"'+' lon="' +strLon + '">'  
               strGpxRow=strGpxRow + "<name>" + strName.replace('"','') + "</name>" + "<sym>" + strSym.replace('"','') + "</sym>"
               if len(strBear)>0: strGpxRow=strGpxRow + "<course>" + strBear + "</course>"
               if len(strEle)>0: strGpxRow=strGpxRow + "<ele>" + strEle + "</ele>"
               if len(strDist)>0 or len(strEleFt)>0:
                  strGpxRow=strGpxRow + "<extensions>"
                  if len(strDist)>0: strGpxRow=strGpxRow + "<distance_nm>" + strDist + "</distance_nm>"
                  if len(strEleFt)>0: strGpxRow=strGpxRow + "<ele_ft>" + strEleFt + "</ele_ft>"
                  strGpxRow=strGpxRow + "</extensions>"

               strGpxRow=strGpxRow + "</"+strType+">"
               strResult=strResult+"\n"+strGpxRow

   if len(strResult)>0:
      txtDisplayContent.backup()
      txtDisplayContent.rupt()
      if strType=="trkpt": strResult="\n" + "<trk><trkseg>" + strResult + "\n" + "</trkseg></trk>" # <name>track</name>
      strResult=cmd.XMLHEADER + "\n" + cmd.GPXROOTNODE%argv[0] + strResult + "\n" + "</gpx>"
      txtDisplayContent.displayContent(strResult)

def mnuToolsMagneticVarMinMaxOnClick():
   iDeclination=None
   strDeclination=""
   floatDeclination=0
   minDeclination=0
   maxDeclination=0
   strMinDeclination=""
   strMaxDeclination=""
   intSign=1
   strEntetes=""
   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   if len(arrContenu)>0:
      strLine=arrContenu[0]
      if csvIsGpxHeader(strLine):
         strEntetes=strLine
         iDeclination=csvIndexOfCol(strLine,"declination")

      if not (iDeclination==None):
         for strLine in arrContenu:
            if csvIsGpxRowOfValues(strLine):
               arrColValues=csvLineToArray(strLine)
               strDeclination=arrColValues[iDeclination]
               if strDeclination.find("W")!=-1: 
                  intSign=-1
               else:
                  intSign=1
               strDeclination=strDeclination.replace("E","")
               strDeclination=strDeclination.replace("W","")
               floatDeclination=float(strDeclination)*intSign
               if minDeclination==0: minDeclination=floatDeclination
               if maxDeclination==0: maxDeclination=floatDeclination
               if floatDeclination<minDeclination: minDeclination=floatDeclination
               if floatDeclination>maxDeclination: maxDeclination=floatDeclination

         if minDeclination<0:
            strMinDeclination=str(minDeclination)+"W"
         else:
            strMinDeclination=str(minDeclination)+"E"

         if maxDeclination<0:
            strMaxDeclination=str(maxDeclination)+"W"
         else:
            strMaxDeclination=str(maxDeclination)+"E"

         msgbox.showinfo(i18n("mnuToolsMagneticVar"),"min: " + strMinDeclination.replace("-","") + "\n" + "max: " + strMaxDeclination.replace("-",""))
      #else:
      #   msgbox.showinfo(i18n("mnuToolsElevation"),"No elevation header")

def mnuToolsMagneticVarConvertJsonToCsvOnClick():
   strJson=txtDisplayContent.getDisplayContent()
   strCard="E"
   strType="W,"
   strCSV="type,latitude,longitude,declination"
   if strJson.find('"result"')>0 and strJson.find('"latitude"')>0 and strJson.find('"longitude"')>0 and strJson.find('"declination"')>0:
      jsonTree=json.loads(strJson)
      for bloc in jsonTree:
         arrElts=bloc["result"] # collection de results dans le bloc en cours
         for arrElt in arrElts:
            if arrElt["declination"]<0: 
               strCard="W"
            else:
               strCard="E"
            strCSV=strCSV+"\n"+strType+str(arrElt["latitude"]) + ","+ str(arrElt["longitude"]) + "," + str(arrElt["declination"]).replace("-","") + strCard
      txtDisplayContent.backup()
      txtDisplayContent.displayContent(strCSV)
   else:
      msg(i18n("msg_declination_json_invalid"))


def mnuToolsMagneticVarDownloadOnClick():
   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   
   arrURL=[]
   strContent=""
   strValues=""
   strUrl=""
   strQry=MAGNETURL_QRY
   result=None
   strResult=""
   TIMEOUT=12 # second
   boolCancel=False
   boolError=False
   boolOk=False
   boolNOk=True 
   boolManual=False
   i=0
   n=0
   t=0
   p=0
   now = cmd.datetime.now()
   month = now.month
   n=len(arrContenu)
   if n>0:
      txtDisplayContent.backup()
      txtDisplayContent.rupt()

      arrURL=[""]*n # tableau d'url
      for strLine in arrContenu:

         if strLine.find("W,")!=-1 or  strLine.find("T,")!=-1:
            p=p+1
            arrValues=strLine.split(",")
            strValues="lat1=" + arrValues[1] + "&lon1=" + arrValues[2] + "&resultFormat=json" + "&month=" + str(month)
            strUrl=strQry + "?" + strValues
            arrURL[p]=strUrl

      i=0
      for strLine in arrURL:
         strUrl=strLine
         if len(strUrl)>1 and not boolCancel and not boolError:
            if i==0:
               i=i+1 
               boolOk=msgbox.askyesno("Question",i18n("msg_download_from")+" "+MAGNETURL_QRY+"\n(timeout="+str(TIMEOUT)+"sec)*"+str(p)+" ?")

            if boolOk:
               boolNOk=False
               txtDisplayContent.displayContent(strUrl)
               pleaseWaitOn()
               time.sleep(1) # attend une seconde (pour éviter le déni de service)
               try:
                  request=urllib2.Request(strUrl,None)
                  result=urllib2.urlopen(request,timeout=TIMEOUT) 
               except IOError as e: 
                  pleaseWaitOff()
                  msg("Warning. "+str(e)+"\n")
                  strContent=""
                  boolError=True
                  if msgbox.askyesno("Question",i18n("msg_download_try_manually")+" ?"):
                     boolManual=True
                  else:
                     boolCancel=True
                     
               else:
                  strResult=result.read().decode("utf-8") # result.read() est un tableau de bytes
                  pleaseWaitOff()
                  if len(strResult): strContent=strContent+strResult # concatène les "results"

      if boolNOk:
         txtDisplayContent.restore()

      if boolCancel:
         txtDisplayContent.restore()

      if boolManual:
         txtDisplayContent.displayContent("\n".join(arrURL))

      if not boolError and len(strContent)>0:
         txtDisplayContent.displayContent("[\n"+strContent.replace("}{","},\n{")+"\n]") # join results par une virgule 
         mnuToolsMagneticVarConvertJsonToCsvOnClick()


def mnuToolsElevationConvertJsonToCsvOnClick():
   strJson=txtDisplayContent.getDisplayContent()
   i=0
   p=0
   l=0
   strType="T,"
   strLat=""
   strLon=""
   strEle=""
   strCSV="type,latitude,longitude,elevation,ele_ft"
   if strJson.find('"results"')>0 and strJson.find('"latitude"')>0 and strJson.find('"longitude"')>0 and strJson.find('"elevation"')>0:
      # Open-Elevation
      jsonTree=json.loads(strJson)
      arrElts=jsonTree["results"]
      for arrElt in arrElts:
         strCSV=strCSV+"\n"+strType+str(arrElt["latitude"])+","+str(arrElt["longitude"])+","+str(arrElt["elevation"])+","+str(int(arrElt["elevation"]*3.3))
         txtDisplayContent.backup()
         txtDisplayContent.displayContent(strCSV)

   elif strJson.find('"results"')>0 and strJson.find('"lat"')>0 and strJson.find('"lng"')>0 and strJson.find('"elevation"')>0:
      # Open-Topo-Data
      jsonTree=json.loads(strJson)
      for bloc in jsonTree:
         arrElts=bloc["results"] # collection de results dans le bloc en cours
         for arrElt in arrElts:
            eleFt=-1
            if isNumeric(str(arrElt["elevation"])): eleFt=int(arrElt["elevation"]*3.3)
            strCSV=strCSV+"\n"+strType+str(arrElt["location"]["lat"])+","+str(arrElt["location"]["lng"])+","+str(arrElt["elevation"])+","+str(eleFt)
      txtDisplayContent.backup()
      txtDisplayContent.displayContent(strCSV)
   else:
      msg(i18n("msg_elevation_json_invalid"))

def mnuToolsElevationOpenFileOnClick():
   arrTypesFiles=[('csv file','.csv'),('json file','.json'),('text file','.txt')]
   strFilename=tkFileDialog.askopenfilename(title=i18n("os_msg_open_file"),filetypes=arrTypesFiles)
   if strFilename:
      txtDisplayContent.backup()
      strContenu=getContenuFichier(strFilename)
      txtDisplayContent.displayContent(strContenu)

def mnuToolsElevationDownloadOnClick():
   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   
   arrURL=[]
   strContent=""
   strValues=""
   strUrl=""
   strQry=DTEDURL_QRY+"?locations="
   result=None
   strResult=""
   strUserAgent="Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
   TIMEOUT=12 # second
   boolNOk=True
   boolCancel=False
   boolError=False
   boolOk=False
   boolManual=False
   i=0
   n=0
   t=0
   p=0
   n=len(arrContenu)
   if n>0:
      txtDisplayContent.backup()
      txtDisplayContent.rupt()
      t=int(n/100)+1 # la longueur d'une l'url est limitée à 4096 caractères, 
      arrURL=[""]*t # tableau d'url en découpant par paquets de 100 points (à environ 40 car. max. par point)
      for strLine in arrContenu:

         if strLine.find("W,")!=-1 or  strLine.find("T,")!=-1:
            p=p+1
            arrValues=strLine.split(",")
            strValues=arrValues[1] + "," + arrValues[2]
            strUrl=strUrl + "|" + strValues
            arrURL[i]=strUrl

            if p>99:
               p=0
               i=i+1
               strUrl=""

      #msg(str(len(arrURL)))
      i=0
      for strLine in arrURL:
         strUrl=strLine[1:] # strUrl.replace("||","")
         if len(strUrl)>1 and not boolCancel and not boolError: # and len(strUrl)<4095
            #encoded_args = urllib.parse.urlencode({'locations' : strUrl}).encode()
            strUrl=strQry+strUrl

            if i==0:
               i=i+1 
               boolOk=msgbox.askyesno("Question",i18n("msg_download_from")+" "+DTEDURL_QRY+"\n(timeout="+str(TIMEOUT)+"sec)*"+str(t)+" ?")

            if boolOk:
               boolNOk=False
               txtDisplayContent.displayContent(strUrl)
               pleaseWaitOn()
               time.sleep(1) # attend une seconde (pour éviter le déni de service sur open-topo-data)
               try:
                  request=urllib2.Request(strUrl,None)
                  #request=urllib2.Request(DTEDURL_QRY,encoded_args)
                  #request.add_header("User-Agent",strUserAgent)
                  result=urllib2.urlopen(request,timeout=TIMEOUT) # data= timeout= hearders=httpHeaders context=cxt
               except IOError as e: 
                  pleaseWaitOff()
                  msg("Warning. "+str(e)+"\n")
                  strContent=""
                  boolError=True
                  if msgbox.askyesno("Question",i18n("msg_download_try_manually")+" ?"):
                     boolManual=True
                  else:
                     boolCancel=True
                     
               else:
                  strResult=result.read().decode("utf-8") # result.read() est un tableau de bytes
                  pleaseWaitOff()
                  if len(strResult): strContent=strContent+strResult # concatène les "results"

         #else:
         #   msg("0 > url < 4094 car. (100 pt.)")

      if boolNOk:
         txtDisplayContent.restore()

      if boolCancel:
         txtDisplayContent.displayContent("\n".join(arrContenu))

      if boolManual:
         txtDisplayContent.displayContent((strQry+("\n"+strQry).join(arrURL)).replace("=|","="))

      if not boolError and len(strContent)>0:
         if DTEDURL_QRY==DTEDURL_OPENELEVATION:
            txtDisplayContent.displayContent(strContent.replace(']}{"results": [',',')) # pour open-elevation, join results par une virgule 
            mnuToolsElevationConvertJsonToCsvOnClick()
         elif DTEDURL_QRY==DTEDURL_OPENTOPODATA:
            txtDisplayContent.displayContent("[\n"+strContent.replace("}\n{","},\n{")+"\n]") # pour open-topo-data, join results par une virgule 
            mnuToolsElevationConvertJsonToCsvOnClick()
            mnuToolsElevationMaximumOnClick()
         else:
            txtDisplayContent.displayContent(strContent)


def mnuToolsElevationMaximumOnClick():
   iEle=None 
   iEleFt=None
   floatME=0
   floatMEFt=0
   floatEle=0
   floatEleFt=0
   strEntetes=""
   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   if len(arrContenu)>0:
      strLine=arrContenu[0]
      if csvIsGpxHeader(strLine):
         strEntetes=strLine
         iEle=csvIndexOfCol(strLine,"elevation")
         iEleFt=csvIndexOfCol(strLine,"ele_ft")

      if not (iEle==None and iEleFt==None):
         for strLine in arrContenu:
            if csvIsGpxRowOfValues(strLine):
               arrColValues=csvLineToArray(strLine)
               if isNumeric(arrColValues[iEle]):
                  floatEle=float(arrColValues[iEle])
                  if floatEle>floatME: floatME=floatEle
                  floatEleFt=float(arrColValues[iEleFt])
                  if floatEleFt>floatMEFt: floatMEFt=floatEleFt

         msgbox.showinfo(i18n("mnuToolsElevation"),"MEF: "+str(int(floatME))+"m - "+str(int(floatMEFt))+"ft")
      #else:
      #   msgbox.showinfo(i18n("mnuToolsElevation"),"No elevation header")

def mnuToolsReplaceBlankRowsByHeaderOnClick():
   # remplace les lignes vierges par le contenu de la ligne 1 (entêtes CSV)
   strEntetesCSV=""
   i=0
   p=0
   l=0
   boolWrite=False
   arrContenu=txtDisplayContent.getTrimDisplayContentArray()
   n=len(arrContenu)-1
   if n>0: strEntetesCSV=arrContenu[0].strip() # copie la première ligne
   if len(strEntetesCSV):
      txtDisplayContent.backup()
      for i in range(n, -1, -1):
         strLine=arrContenu[i].strip()
         p=str(i+1)
         l=len(strLine)
         if boolWrite and l==0:
               txtDisplayContent.delete(p+".0",p+".0")
               txtDisplayContent.insert(p+".0",strEntetesCSV)
         boolWrite=(l>0) and (strLine!=strEntetesCSV)

def mnuToolsContentRowsCountOnClick():
   txtDisplayContent.rows_count()

def mnuEditContentCopyOnClick():
   if getTabIndex()==0: txtDisplayContent.copy()
   if getTabIndex()==1: txtSandbox.copy()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.copy()

def mnuEditContentCutOnClick():
   if getTabIndex()==0: 
      txtDisplayContent.backup()
      txtDisplayContent.cut()
   if getTabIndex()==1:
      txtSandbox.cut()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.cut()

def mnuEditContentPastOnClick():
   if getTabIndex()==0:
      txtDisplayContent.backup()
      txtDisplayContent.past()
   if getTabIndex()==1: txtSandbox.past()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.past()

def mnuEditContentCancelOnClick():
   if getTabIndex()==0: 
      txtDisplayContent.restore()
   if getTabIndex()==1:
      txtSandbox.cancel()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.cancel()

def mnuEditContentSelectRowOnClick():
   if getTabIndex()==0: txtDisplayContent.select_row()
   if getTabIndex()==1: txtSandbox.select_row()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.select_row()

def mnuEditContentInsertRowOnClick():
   if getTabIndex()==0:
      txtDisplayContent.backup()
      txtDisplayContent.insert_row()
   if getTabIndex()==1: txtSandbox.insert_row()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.insert_row()

def mnuEditContentCutRowOnClick():
   if getTabIndex()==0:
      txtDisplayContent.backup()
      txtDisplayContent.cut_row()
   if getTabIndex()==1: txtSandbox.cut_row()

def mnuEditContentSelectAllOnClick():
   if getTabIndex()==0: txtDisplayContent.select_all()
   if getTabIndex()==1: txtSandbox.select_all()
   #if txtContenuFichierTexte.focus_get(): txtContenuFichierTexte.select_all()

# ----------------------------------------------------------------------------
# Bind
# ----------------------------------------------------------------------------
def nbkMainOnTabChanged(event):
   #msg(event.widget.index('current'))
   if getTabIndex()==0: txtDisplayContent.focus_set()
   if getTabIndex()==1: txtSandbox.focus_set()

def btnEditContentMoveRowUp(event):
   if getTabIndex()==0: 
      txtDisplayContent.backup()
      txtDisplayContent.move_row_up()
   if getTabIndex()==1: txtSandbox.move_row_up()
   
def btnEditContentMoveRowDn(event):
   if getTabIndex()==0: 
      txtDisplayContent.backup()
      txtDisplayContent.move_row_dn()
   if getTabIndex()==1: txtSandbox.move_row_dn()

# ----------------------------------------------------------------------------
if __name__ == '__main__':
   gLOCALE=setLOCALE()
   #print("LOCALE",gLOCALE)
 
   # initialisation de la fenêtre principale
   mainWindow = tk.Tk()
   mainWindow.title(cheminFichier())

   # ----------------------------------------
   # création des menus
   # ----------------------------------------
   menubar = tk.Menu(mainWindow)

   mnuApp = tk.Menu(menubar, tearoff=0)
   mnuApp.add_command(label=i18n("mnuAppShowConfig"), command=mnuAppShowConfigOnClick)
   mnuApp.add_command(label=i18n("mnuAppSaveConfig"), command=mnuAppSaveConfigOnClick)
   mnuApp.add_command(label=i18n("mnuAppResetConfig"), command=mnuAppResetConfigOnClick)
   mnuApp.add_separator()
   mnuApp.add_command(label=i18n("mnuAppOpenFile"), command=mnuAppOpenFileOnClick)
   mnuApp.add_command(label=i18n("mnuAppSaveContentAs"), command=mnuAppSaveContentAsOnClick)
   mnuApp.add_separator()
   mnuApp.add_command(label=i18n("mnuAppHelp"), command=mnuAppHelpOnClick)
   mnuApp.add_command(label=i18n("mnuAppAbout"), command=mnuAppAboutOnClick)
   mnuApp.add_separator()
   mnuApp.add_command(label=i18n("mnuAppExit"), command=mainWindow.quit)

   mnuData = tk.Menu(menubar, tearoff=0)
   mnuData.add_command(label=i18n("mnuDataShowDBFile"), command=mnuDataShowDBFileOnClick)
   mnuData.add_command(label=i18n("mnuDataConvertDBFileToCSV"), command=mnuDataConvertDBFileToCSVOnClick)
   mnuData.add_command(label=i18n("mnuDataExtractDBFile"), command=mnuDataExtractDBFileOnClick)
   mnuData.add_separator()
   mnuData.add_command(label=i18n("mnuDataSaveAsDBFileList"), command=mnuDataSaveAsDBFileListOnClick)
   mnuData.add_command(label=i18n("mnuDataDeleteDBFile"), command=mnuDataDeleteDBFileOnClick)
   mnuData.add_separator()
   mnuData.add_command(label=i18n("mnuDataRefreshDBFileList"), command=mnuDataRefreshDBFileListOnClick)
   
   mnuSearch = tk.Menu(menubar, tearoff=0)
   mnuSearch.add_command(label=i18n("mnuSearchSearch"), command=mnuSearchSearchOnClick)
   mnuSearch.add_separator()
   mnuSearch.add_command(label=i18n("mnuSearchShowCSV"), command=mnuSearchShowCSVOnClick)
   mnuSearch.add_command(label=i18n("mnuSearchShowGPX"), command=mnuSearchShowGPXOnClick)
   mnuSearch.add_command(label=i18n("mnuSearchShowXML"), command=mnuSearchShowXMLOnClick)
   mnuSearch.add_separator()
   mnuSearch.add_command(label=i18n("mnuSearchShowLog"), command=mnuSearchShowLogOnClick)

   mnuEditContent = tk.Menu(menubar, tearoff=0)
   mnuEditContent.add_command(label=i18n("mnuEditContentCopy"), command=mnuEditContentCopyOnClick)
   mnuEditContent.add_command(label=i18n("mnuEditContentCut"), command=mnuEditContentCutOnClick)
   mnuEditContent.add_command(label=i18n("mnuEditContentPast"), command=mnuEditContentPastOnClick)
   mnuEditContent.add_separator()
   mnuEditContent.add_command(label=i18n("mnuEditContentInsertRow"), command=mnuEditContentInsertRowOnClick)
   mnuEditContent.add_command(label=i18n("mnuEditContentCutRow"), command=mnuEditContentCutRowOnClick)
   mnuEditContent.add_command(label=i18n("mnuEditContentSelectRow"), command=mnuEditContentSelectRowOnClick)
   mnuEditContent.add_command(label=i18n("mnuEditContentSelectAll"), command=mnuEditContentSelectAllOnClick)
   mnuEditContent.add_separator()
   mnuEditContent.add_command(label=i18n("mnuEditContentCancel"), command=mnuEditContentCancelOnClick)
   
   mnuTools = tk.Menu(menubar, tearoff=0)
   mnuTools.add_command(label=i18n("mnuToolsDist2Points"), command=mnuToolsDist2PointsOnClick)
   mnuTools.add_command(label=i18n("mnuToolsIntermediatePt"), command=mnuToolsIntermediatePtOnClick)
   mnuTools.add_command(label=i18n("mnuToolsCoordByBearingDist"), command=mnuToolsCoordByBearingDistOnClick)
   mnuTools.add_command(label=i18n("mnuToolsMagnetDeclin"), command=mnuToolsMagnetDeclinOnClick)
   mnuToolsMagneticVar = tk.Menu(mnuTools, tearoff=0)
   mnuToolsMagneticVar.add_command(label=i18n("mnuToolsMagneticVarDownload"), command=mnuToolsMagneticVarDownloadOnClick)
   mnuToolsMagneticVar.add_command(label=i18n("mnuToolsMagneticVarMinMax"), command=mnuToolsMagneticVarMinMaxOnClick)
   mnuToolsMagneticVar.add_command(label=i18n("mnuToolsMagneticVarConvertJsonToCsv"), command=mnuToolsMagneticVarConvertJsonToCsvOnClick)
   mnuToolsElevation = tk.Menu(mnuTools, tearoff=0)
   mnuToolsElevation.add_command(label=i18n("mnuToolsElevationDownload"), command=mnuToolsElevationDownloadOnClick)
   #mnuToolsElevation.add_command(label=i18n("mnuToolsElevationOpenFile"), command=mnuToolsElevationOpenFileOnClick)
   mnuToolsElevation.add_command(label=i18n("mnuToolsElevationMaximum"), command=mnuToolsElevationMaximumOnClick)
   mnuToolsElevation.add_command(label=i18n("mnuToolsElevationConvertJsonToCsv"), command=mnuToolsElevationConvertJsonToCsvOnClick)
   mnuTools.add_cascade(label=i18n("mnuToolsMagneticVar")+"...",menu=mnuToolsMagneticVar)
   mnuTools.add_cascade(label=i18n("mnuToolsElevation")+"...",menu=mnuToolsElevation)
   mnuTools.add_separator()
   mnuTools.add_command(label=i18n("mnuToolsConvertCsvToGpx"), command=mnuToolsConvertCsvToGpxOnClick)
   mnuTools.add_separator()
   mnuTools.add_command(label=i18n("mnuToolsReplaceBlankRowsByHeader"), command=mnuToolsReplaceBlankRowsByHeaderOnClick)
   mnuTools.add_command(label=i18n("mnuToolsContentRowsCount"), command=mnuToolsContentRowsCountOnClick)

   # instancie la menubar
   menubar.add_cascade(label=i18n("mnuApp"),menu=mnuApp)
   menubar.add_cascade(label=i18n("mnuData"),menu=mnuData)
   menubar.add_cascade(label=i18n("mnuSearch"),menu=mnuSearch)
   menubar.add_cascade(label=i18n("mnuTools"),menu=mnuTools)
   menubar.add_cascade(label=i18n("mnuEditContent"),menu=mnuEditContent)
   mainWindow.config(menu=menubar)

   # ----------------------------------------
   # disposition des champs dans des panels
   # ----------------------------------------
   # dimensionnement de la fenêtre
   windowWidth=1270
   if windowWidth>mainWindow.winfo_screenwidth(): windowWidth=mainWindow.winfo_screenwidth()

   windowHeight=660
   if windowHeight>mainWindow.winfo_screenheight(): windowHeight=mainWindow.winfo_screenheight()

   # ----------------------------------------
   # panel top
   PANBG="darkgray" # couleur de fond par défaut
   panMain= tk.PanedWindow(mainWindow, orient=tk.VERTICAL,bg=PANBG)

   # panel Top / barre de boutons
   WB=10 # largeur par défaut des boutons
   RLF=tk.FLAT
   panButtons = tk.PanedWindow(panMain, orient=tk.HORIZONTAL,bg=PANBG)

   frmButtons=tk.Frame(panButtons,bg=PANBG) # ,borderwidth=1,relief=tk.GROOVE
   frmButtons.grid()

   btnAppQuit=tk.Button(frmButtons,text=i18n("btnAppQuit"), width=WB, relief=RLF,command=mainWindow.quit)
   btnSearchSearch=tk.Button(frmButtons,text=i18n("btnSearchSearch"), width=WB, relief=RLF,command=mnuSearchSearchOnClick)
   btnSearchShowCSV=tk.Button(frmButtons,text="R", relief=RLF,command=mnuSearchShowCSVOnClick)

   btnToolsDist2Points=tk.Button(frmButtons,text=i18n("btnToolsDist2Points"), width=WB, relief=RLF,command=mnuToolsDist2PointsOnClick)
   btnToolsCoordByBearingDist=tk.Button(frmButtons,text=i18n("btnToolsCoordByBearingDist"), width=WB, relief=RLF,command=mnuToolsCoordByBearingDistOnClick)
   btnToolsMagnetDeclin=tk.Button(frmButtons,text=i18n("btnToolsMagnetDeclin"), width=WB, relief=RLF,command=mnuToolsMagnetDeclinOnClick)

   btnEditContentCopy=tk.Button(frmButtons,text="C", relief=RLF,command=mnuEditContentCopyOnClick)
   btnEditContentCut=tk.Button(frmButtons,text="X", relief=RLF,command=mnuEditContentCutOnClick)
   btnEditContentPast=tk.Button(frmButtons,text="V", relief=RLF,command=mnuEditContentPastOnClick)
   btnEditContentInsertRow=tk.Button(frmButtons,text="I", relief=RLF,command=mnuEditContentInsertRowOnClick)
   btnEditContentCutRow=tk.Button(frmButtons,text="K", relief=RLF,command=mnuEditContentCutRowOnClick)
   btnEditContentSelectRow=tk.Button(frmButtons,text="L", relief=RLF,command=mnuEditContentSelectRowOnClick)
   btnEditContentSelectAll=tk.Button(frmButtons,text="A", relief=RLF,command=mnuEditContentSelectAllOnClick)
   btnEditContentCancel=tk.Button(frmButtons,text="Z", relief=RLF,command=mnuEditContentCancelOnClick)

   btnAppQuit.grid(row=0,column=0,padx=4)
   btnSearchSearch.grid(row=0,column=1,padx=1)
   btnSearchShowCSV.grid(row=0,column=2,padx=1)

   btnToolsDist2Points.grid(row=0,column=3,padx=4)
   btnToolsCoordByBearingDist.grid(row=0,column=4)
   btnToolsMagnetDeclin.grid(row=0,column=5,padx=4)

   btnEditContentCopy.grid(row=0,column=6,padx=3)
   btnEditContentCut.grid(row=0,column=7,padx=0)
   btnEditContentPast.grid(row=0,column=8,padx=3)
   btnEditContentInsertRow.grid(row=0,column=9,padx=2)
   btnEditContentCutRow.grid(row=0,column=10,padx=1)
   btnEditContentSelectRow.grid(row=0,column=11,padx=2)
   btnEditContentSelectAll.grid(row=0,column=12,padx=3)
   btnEditContentCancel.grid(row=0,column=13,padx=2)
   #btnEditContentRowsCount.grid(row=0,column=10,padx=2)

   panButtons.add(frmButtons)
   panButtons.pack(fill=tk.BOTH,padx=3, pady=5)

   # panel Top/info
   panInfo = tk.PanedWindow(panMain, orient=tk.HORIZONTAL)

   frmInfo=tk.Frame(panInfo) # ,borderwidth=1,relief=tk.GROOVE
   frmInfo.grid() #frmInfo.columnconfigure(0,weight=1)

   # Libellé 1
   lblInfoLib1 = tk.Label(frmInfo, text=i18n("lblInfoLib1")+":") # anchor:W = text-align:left

   # Label contenant le fichier source sélectionné par l'utilisateur
   lblSourceFilename = tk.Label(frmInfo, text="()",font="-size 9 -weight bold") # anchor='w'

   # Libellé 2 contiendra par exemple le "please wait" pour les longs traitements
   lblInfoLib2 = tk.Label(frmInfo, text=" ",fg="red")

   # Positionnement des champs dans le Frame

   lblInfoLib1.grid(row=0,column=0)
   lblSourceFilename.grid(row=0,column=1)
   lblInfoLib2.grid(row=0,column=2)

   frmInfo.columnconfigure(2,weight=3) # attribution de l'espace de la dernière colonne 

   # disposition des champs dans le panel 
   panInfo.add(frmInfo)
   panInfo.pack(fill=tk.BOTH,padx=2, pady=0) # expand=tk.Y


   # --- panel 2 ---
   panData = tk.PanedWindow(panMain, orient=tk.HORIZONTAL)

   # panel gauche
   panLeft = tk.PanedWindow(panData, orient=tk.VERTICAL,width=270)

   # liste des fichiers sources de données 
   lstFichiers = tk.Listbox(panLeft,height=20) # ,width=30

   # radiobuttons pour choix de type de points
   frmChoixType=tk.LabelFrame(panLeft,borderwidth=0,relief=tk.FLAT,text=i18n("frmChoixType")+':')
   frmChoixType.grid()
   strRbtnTypeId=tk.StringVar()
   rbtnTypeW=tk.Radiobutton(frmChoixType,text="Waypoint",variable=strRbtnTypeId,value="W",command=rbtnTypeOnChange)
   rbtnTypeT=tk.Radiobutton(frmChoixType,text="Trackpoint",variable=strRbtnTypeId,value="T",command=rbtnTypeOnChange)
   rbtnTypeW.select()
   rbtnTypeW.grid(row=0,column=0)
   rbtnTypeT.grid(row=0,column=1)

   # champ texte contenant le fichier texte
   txtContenuFichierTexte = PopText(mainWindow) # tk.Text()

   # disposition des champs dans le panel gauche
   panLeft.add(lstFichiers)
   panLeft.add(frmChoixType)
   panLeft.add(txtContenuFichierTexte)

   # panel droit
   nbkMain = ttk.Notebook(panData)
   panRight = tk.PanedWindow(nbkMain)
   panRight2 = tk.PanedWindow(nbkMain)

   # champ texte principal pour afficher les données
   txtDisplayContent = PopText(mainWindow) #tk.Text()
   txtSandbox = PopText(mainWindow) #tk.Text()

   # disposition des champs dans le panel droit
   panRight.add(txtDisplayContent)
   panRight2.add(txtSandbox)
   nbkMain.add(panRight,text=i18n("nbkContentLib"))
   nbkMain.add(panRight2,text=i18n("nbkSandboxLib"))
   nbkMain.pack()
   nbkMain.bind("<<NotebookTabChanged>>",nbkMainOnTabChanged)


   # disposition des sous-panels dans le panel 
   panData.add(panLeft)
   #panData.add(panRight)
   panData.add(nbkMain)
   panData.pack(expand=tk.Y,fill=tk.BOTH,padx=2, pady=0)

   # pack panMain
   panMain.pack(expand=tk.Y,fill=tk.BOTH,pady=1)

   # init et bind events des champs (en fin en raison d''interactions évènementielles entre champs)
   lstFichiers.bind("<<ListboxSelect>>", lstFichiersOnSelect)
   lstFichiersInit()
   txtContenuFichierTexte.insert("1.0",getContenuFichierTexte())
   
   #btnEditContentMoveRow.bind()
   btnEditContentSelectRow.bind("<Double-Button-1>", btnEditContentMoveRowUp)
   btnEditContentSelectRow.bind("<Double-Button-3>", btnEditContentMoveRowDn)

   # affichage de la fenêtre principale
   mainWindow.attributes("-fullscreen",False)
   #strWindowGeometry=str(windowWidth)+"x"+str(windowHeight)
   #print(strWindowGeometry)
   #mainWindow.geometry(strWindowGeometry)
   mainWindow.mainloop()

