# groute
(en) Search the coordinates of points and plan a route
![screenshot](https://gitlab.com/ledudulela/groute/-/raw/master/groute_ver_en.jpg)

First, unzip FGFS_DB.zip (Flightgear database) in the directory of the application.

---

(fr) Rechercher les coordonnées de points et planifier un itinéraire
![screenshot](https://gitlab.com/ledudulela/groute/-/raw/master/groute_ver_fr.jpg)

Tout d'abord, dézippez FGFS_DB.zip (Base de données Flightgear) dans le répertoire de l'application.
